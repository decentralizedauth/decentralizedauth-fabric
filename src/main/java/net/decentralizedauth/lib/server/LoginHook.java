package net.decentralizedauth.lib.server;

import com.mojang.authlib.GameProfile;

public interface LoginHook {
  // TODO don't use GameProfile here as it is specific to the Fabric decompiled
  // mappings
  public GameProfile setupPlayer(GameProfile requestedProfile, ProfileLookup profileLookup) throws DenyLoginException;

  public static boolean validVanillaUsername(String username) {
    return username.length() >= 3 && username.length() <= 16 && username.chars().allMatch(c -> ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9') || (c == '_')));
  }
}
