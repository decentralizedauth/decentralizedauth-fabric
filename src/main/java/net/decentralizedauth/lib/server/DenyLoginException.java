package net.decentralizedauth.lib.server;

public class DenyLoginException extends Exception {
  public DenyLoginException(String reason) {
    super(reason);
  }
}
