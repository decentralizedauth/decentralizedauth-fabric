package net.decentralizedauth.lib.server;

import java.util.Optional;
import java.util.UUID;

import com.mojang.authlib.GameProfile;

public interface ProfileLookup {
  // TODO don't use `GameProfile` here as it is specific to the fabric
  // decompiled mappings.
  public Optional<GameProfile> cachedLoginForUUID(UUID id);
  public Optional<GameProfile> cachedLoginWithName(String name);
  public Optional<GameProfile> mojangProfileWithName(String name);
  public void evictCacheEntry(UUID id);
}
