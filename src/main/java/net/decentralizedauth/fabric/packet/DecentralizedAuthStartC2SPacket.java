package net.decentralizedauth.fabric.packet;

import net.decentralizedauth.fabric.server.ServerDecentralizedAuthPacketListener;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

public record DecentralizedAuthStartC2SPacket() implements Packet<ServerDecentralizedAuthPacketListener> {
  public DecentralizedAuthStartC2SPacket(PacketByteBuf buf) {
    this();
  }

  @Override
  public void write(PacketByteBuf buf) {
  }

  @Override
  public void apply(ServerDecentralizedAuthPacketListener serverDecentralizedAuthPacketListener) {
    serverDecentralizedAuthPacketListener.onStart(this);
  }
}
