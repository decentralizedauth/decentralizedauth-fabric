package net.decentralizedauth.fabric.packet;

import java.util.HashMap;
import java.util.Map;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;

import net.decentralizedauth.fabric.server.ServerDecentralizedAuthPacketListener;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

public class DecentralizedAuthProfileResponseC2SPacket implements Packet<ServerDecentralizedAuthPacketListener> {
  private GameProfile profile;
  private HashMap<MinecraftProfileTexture.Type, byte[]> textureData;

  public DecentralizedAuthProfileResponseC2SPacket(GameProfile profile, HashMap<MinecraftProfileTexture.Type, byte[]> textureData) {
    this.profile = profile;
    this.textureData = textureData;
  }

  public DecentralizedAuthProfileResponseC2SPacket(PacketByteBuf buf) {
    this.profile = buf.readGameProfile();
    this.textureData = new HashMap<MinecraftProfileTexture.Type, byte[]>();
    buf.forEachInCollection(innerBuf -> {
      String textureTypeString = innerBuf.readString();
      MinecraftProfileTexture.Type textureType = MinecraftProfileTexture.Type.valueOf(textureTypeString);
      if (textureType == MinecraftProfileTexture.Type.ELYTRA) {
        // Elytra textures are embedded in the cape texture; not used in practice
        throw new IllegalArgumentException("Invalid texture type 'ELYTRA'");
      }
      // Forbid duplicate types
      if (this.textureData.containsKey(textureType)) {
        throw new IllegalStateException("Protocol error");
      }
      byte[] textureBytes = innerBuf.readByteArray(16384);
      this.textureData.put(textureType, textureBytes);
    });
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeGameProfile(this.profile);
    buf.writeCollection(this.textureData.entrySet(), (innerBuf, entry) -> {
      innerBuf.writeString(entry.getKey().name());
      innerBuf.writeByteArray(entry.getValue());
    });
  }

  @Override
  public void apply(ServerDecentralizedAuthPacketListener serverDecentralizedAuthPacketListener) {
    serverDecentralizedAuthPacketListener.onProfileResponse(this);
  }

  public GameProfile getProfile() {
    return this.profile;
  }

  public Map<MinecraftProfileTexture.Type, byte[]> getTextureData() {
    return this.textureData;
  }
}
