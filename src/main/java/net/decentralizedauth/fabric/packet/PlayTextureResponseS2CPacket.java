package net.decentralizedauth.fabric.packet;

import net.minecraft.network.PacketByteBuf;

public class PlayTextureResponseS2CPacket {
  byte[] textureData;

  public PlayTextureResponseS2CPacket(byte[] textureData) {
    this.textureData = textureData;
  }

  public PlayTextureResponseS2CPacket(PacketByteBuf buf) {
    this.textureData = buf.readByteArray(16384);
  }

  public void write(PacketByteBuf buf) {
    buf.writeByteArray(this.textureData);
  }

  public byte[] getTextureData() {
    return this.textureData;
  }
}
