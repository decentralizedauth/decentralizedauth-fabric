package net.decentralizedauth.fabric.packet;

import javax.crypto.SecretKey;

import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;

import net.decentralizedauth.fabric.client.ClientDecentralizedAuthPacketListener;
import net.decentralizedauth.fabric.util.DecentralizedAuthNetworkEncryptionUtils;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

public class DecentralizedAuthAuthChallengeS2CPacket implements Packet<ClientDecentralizedAuthPacketListener> {
  private final byte[] serverIdentityPublicKey;
  private final byte[] signedSharedSecret;

  public DecentralizedAuthAuthChallengeS2CPacket(Ed25519PublicKeyParameters serverIdentityPublicKey, byte[] signedSharedSecret) {
    this.serverIdentityPublicKey = serverIdentityPublicKey.getEncoded();
    this.signedSharedSecret = signedSharedSecret;
  }

  public DecentralizedAuthAuthChallengeS2CPacket(PacketByteBuf buf) {
    this.serverIdentityPublicKey = buf.readByteArray();
    this.signedSharedSecret = buf.readByteArray();
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeByteArray(this.serverIdentityPublicKey);
    buf.writeByteArray(this.signedSharedSecret);
  }

  @Override
  public void apply(ClientDecentralizedAuthPacketListener packetListener) {
    packetListener.onAuthChallenge(this);
  }

  public Ed25519PublicKeyParameters getServerIdentityPublicKey() {
    return new Ed25519PublicKeyParameters(this.serverIdentityPublicKey, 0);
  }

  public boolean verifySignedSharedSecret(SecretKey sharedSecret) {
    return DecentralizedAuthNetworkEncryptionUtils.verifyIdentityProof(this.getServerIdentityPublicKey(), this.signedSharedSecret, sharedSecret);
  }
}
