package net.decentralizedauth.fabric.packet;

import java.security.PublicKey;

import net.decentralizedauth.fabric.server.ServerDecentralizedAuthPacketListener;
import net.decentralizedauth.fabric.util.DecentralizedAuthNetworkEncryptionUtils;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.encryption.NetworkEncryptionException;

public class DecentralizedAuthEncryptionResponseC2SPacket
implements Packet<ServerDecentralizedAuthPacketListener> {
  private final byte[] publicKey;

  public DecentralizedAuthEncryptionResponseC2SPacket(PublicKey publicKey) throws NetworkEncryptionException {
    this.publicKey = publicKey.getEncoded();
  }

  public DecentralizedAuthEncryptionResponseC2SPacket(PacketByteBuf buf) {
    this.publicKey = buf.readByteArray();
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeByteArray(this.publicKey);
  }

  @Override
  public void apply(ServerDecentralizedAuthPacketListener serverLoginPacketListener) {
    serverLoginPacketListener.onEncryptionResponse(this);
  }

  public PublicKey getPublicKey() throws NetworkEncryptionException {
    return DecentralizedAuthNetworkEncryptionUtils.decodeEncodedX25519PublicKey(this.publicKey);
  }
}
