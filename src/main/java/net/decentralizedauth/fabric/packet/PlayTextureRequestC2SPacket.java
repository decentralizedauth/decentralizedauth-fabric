package net.decentralizedauth.fabric.packet;

import java.util.HexFormat;

import org.apache.commons.lang3.Validate;
import org.bouncycastle.util.encoders.Hex;

import net.minecraft.network.PacketByteBuf;

public class PlayTextureRequestC2SPacket {
  byte[] textureSha256;

  public PlayTextureRequestC2SPacket(String textureSha256String) {
    this.textureSha256 = HexFormat.of().withLowerCase().parseHex(textureSha256String);
    Validate.validState(this.textureSha256.length == 32, "invalid SHA256 hash", new Object[0]);
  }

  public PlayTextureRequestC2SPacket(PacketByteBuf buf) {
    this.textureSha256 = buf.readByteArray(32);
  }

  public void write(PacketByteBuf buf) {
    buf.writeByteArray(this.textureSha256);
  }

  public String getTextureSha256() {
    return new String(Hex.encode(this.textureSha256));
  }
}
