package net.decentralizedauth.fabric.packet;

import java.security.PublicKey;

import net.decentralizedauth.fabric.client.ClientDecentralizedAuthPacketListener;
import net.decentralizedauth.fabric.util.DecentralizedAuthNetworkEncryptionUtils;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.network.encryption.NetworkEncryptionException;

public class DecentralizedAuthEncryptionRequestS2CPacket implements Packet<ClientDecentralizedAuthPacketListener> {
  private final byte[] publicKey;

  public DecentralizedAuthEncryptionRequestS2CPacket(PublicKey publicKey) {
    this.publicKey = publicKey.getEncoded();
  }

  public DecentralizedAuthEncryptionRequestS2CPacket(PacketByteBuf buf) {
    this.publicKey = buf.readByteArray();
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeByteArray(this.publicKey);
  }

  @Override
  public void apply(ClientDecentralizedAuthPacketListener packetListener) {
    packetListener.onEncryptionRequest(this);
  }

  public PublicKey getPublicKey() throws NetworkEncryptionException {
    return DecentralizedAuthNetworkEncryptionUtils.decodeEncodedX25519PublicKey(this.publicKey);
  }
}
