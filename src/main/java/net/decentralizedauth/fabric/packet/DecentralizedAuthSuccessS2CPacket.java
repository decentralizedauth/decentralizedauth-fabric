package net.decentralizedauth.fabric.packet;

import com.mojang.authlib.GameProfile;

import net.decentralizedauth.fabric.client.ClientDecentralizedAuthPacketListener;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

public class DecentralizedAuthSuccessS2CPacket
implements Packet<ClientDecentralizedAuthPacketListener> {
  private final GameProfile profile;

  public DecentralizedAuthSuccessS2CPacket(GameProfile profile) {
    this.profile = profile;
  }

  public DecentralizedAuthSuccessS2CPacket(PacketByteBuf buf) {
    this.profile = buf.readGameProfile();
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeGameProfile(this.profile);
  }

  @Override
  public void apply(ClientDecentralizedAuthPacketListener packetListener) {
    packetListener.onSuccess(this);
  }

  public GameProfile getProfile() {
    return this.profile;
  }
}
