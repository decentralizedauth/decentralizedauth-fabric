package net.decentralizedauth.fabric.packet;

import java.util.Optional;

import com.mojang.authlib.GameProfile;

import net.decentralizedauth.fabric.client.ClientDecentralizedAuthPacketListener;
import net.minecraft.network.Packet;
import net.minecraft.network.PacketByteBuf;

public class DecentralizedAuthProfileRequestS2CPacket
implements Packet<ClientDecentralizedAuthPacketListener> {
  private final Optional<GameProfile> lastIdentity;

  public DecentralizedAuthProfileRequestS2CPacket(Optional<GameProfile> lastIdentity) {
    this.lastIdentity = lastIdentity;
  }

  public DecentralizedAuthProfileRequestS2CPacket(PacketByteBuf buf) {
    this.lastIdentity = buf.readOptional(buf2 -> buf2.readGameProfile());
  }

  @Override
  public void write(PacketByteBuf buf) {
    buf.writeOptional(this.lastIdentity, (buf2, lastIdentity) -> buf.writeGameProfile(lastIdentity));
  }

  @Override
  public void apply(ClientDecentralizedAuthPacketListener packetListener) {
    packetListener.onProfileRequest(this);
  }

  public Optional<GameProfile> getLastIdentity() {
    return this.lastIdentity;
  }
}
