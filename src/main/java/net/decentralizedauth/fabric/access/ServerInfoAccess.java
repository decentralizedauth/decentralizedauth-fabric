package net.decentralizedauth.fabric.access;

public interface ServerInfoAccess {
  public void setDecentralizedAuthSupport(boolean supported);
  public boolean getDecentralizedAuthSupport();
}
