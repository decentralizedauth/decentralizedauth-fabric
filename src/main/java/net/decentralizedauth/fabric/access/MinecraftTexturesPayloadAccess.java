package net.decentralizedauth.fabric.access;

import java.util.Map;
import java.util.UUID;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;

public interface MinecraftTexturesPayloadAccess {
  public void setTimestamp(long timestamp);

  public void setProfileId(UUID profileId);

  public void setProfileName(String profileName);

  public void setIsPublic(boolean isPublic);

  public void setTextures(Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> textures);
}
