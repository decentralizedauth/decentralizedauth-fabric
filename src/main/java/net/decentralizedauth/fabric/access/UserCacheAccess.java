package net.decentralizedauth.fabric.access;

import java.util.Optional;
import java.util.UUID;

import com.mojang.authlib.GameProfile;

public interface UserCacheAccess {
  /**
   * Similar to `UserCache.findByName`, but doesn't fallback to Mojang's
   * servers if nothing is found locally.
   */
  public Optional<GameProfile> findCachedByName(String name);

  /**
   * Similar to `UserCache.findByName`, but only checks Mojang's servers
   * without checking the local cache.
   */
  public Optional<GameProfile> findOfficialAccountByName(String name);

  public void removeByUUID(UUID id);
}
