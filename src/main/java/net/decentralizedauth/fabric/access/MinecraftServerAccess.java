package net.decentralizedauth.fabric.access;

import java.io.File;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.jetbrains.annotations.Nullable;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;

public interface MinecraftServerAccess {
  public void setDecentralizedAuthIdentity(Ed25519PrivateKeyParameters id);

  @Nullable
  public Ed25519PrivateKeyParameters getDecentralizedAuthIdentity();

  public File getDecentralizedAuthClientDirectory();

  public File getServerTextureCachePath(MinecraftProfileTexture.Type type, String textureSha256);
}
