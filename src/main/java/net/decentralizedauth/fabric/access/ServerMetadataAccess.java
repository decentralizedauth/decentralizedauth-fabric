package net.decentralizedauth.fabric.access;

import org.jetbrains.annotations.Nullable;

public interface ServerMetadataAccess {
  public void setDecentralizedAuthSupport(Integer protocol);

  @Nullable
  public Integer getDecentralizedAuthSupport();
}
