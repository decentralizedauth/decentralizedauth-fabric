package net.decentralizedauth.fabric.access;

public interface PlayerSkinProviderAccess {
  void onReceiveTextureFromServer(byte[] textureData);
  void registerPendingTextureRequest(String textureSha256, PlayerSkinTextureAccess pendingTexture);
}
