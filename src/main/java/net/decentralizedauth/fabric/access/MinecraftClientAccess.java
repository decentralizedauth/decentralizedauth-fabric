package net.decentralizedauth.fabric.access;

import java.io.File;
import java.util.Map;
import java.util.Optional;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.jetbrains.annotations.Nullable;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;

import net.decentralizedauth.fabric.client.DecentralizedAuthKnownServers;

public interface MinecraftClientAccess {
  public void setDecentralizedAuthIdentity(Ed25519PrivateKeyParameters id);

  @Nullable
  public Ed25519PrivateKeyParameters getDecentralizedAuthIdentity();

  public File getDecentralizedAuthClientDirectory();

  public DecentralizedAuthKnownServers getDecentralizedAuthKnownServers();

  public void prepareTextureData(MinecraftProfileTexture.Type textureType, Optional<GameProfile> cachedIdentity, Map<MinecraftProfileTexture.Type, String> textureHashes, Map<MinecraftProfileTexture.Type, byte[]> newTextureData);
}
