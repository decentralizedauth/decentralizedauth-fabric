package net.decentralizedauth.fabric.access;

public interface PlayerSkinTextureAccess {
  public void setTextureSha256(String sha256);
  public void setPlayerSkinProvider(PlayerSkinProviderAccess provider);
  public void onTextureLoadedFromServer(byte[] textureData);
}
