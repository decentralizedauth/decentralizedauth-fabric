package net.decentralizedauth.fabric;

import java.security.Security;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.minecraft.util.Identifier;

public class DecentralizedAuth implements ModInitializer {
  public static final Logger LOGGER = LoggerFactory.getLogger("decentralizedauth");

  public static final Identifier TEXTURES_CHANNEL = new Identifier("decentralizedauth", "textures");

  @Override
  public void onInitialize() {
    LOGGER.info("Decentralized Auth injected.");
    Security.addProvider(new BouncyCastleProvider());
    CommandRegistrationCallback.EVENT.register((dispatcher, registry, dedicated) -> {
      DecentralizedAuthCommand.register(dispatcher, dedicated.dedicated);
    });
  }
}
