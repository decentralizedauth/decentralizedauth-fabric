package net.decentralizedauth.fabric.util;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Objects;
import java.util.UUID;

import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;

/**
 *  Minecraft uses Type 4 UUIDs, whereas Type 5 UUIDs are content-based and
 *  therefore better for a decentralized system. Type 4 and Type 5 UUIDs will
 *  never collide because the 13th hex digit is hardcoded at `4` and `5`,
 *  respectively. Type 5 UUIDs are not implemented by default in any Java
 *  standard libraries, so this reimplementation is based off of (this
 *  StackOverflow answer)[https://stackoverflow.com/a/40230410]. The
 *  implementation uses SHA-1, which is no longer secure against collisions;
 *  this is mitigated by calculating the UUID using a longer user public key
 *  that is cryptographically verified by the server before a player may join.
 */
public class UUIDv5 {
    private static final Charset UTF8 = Charset.forName("UTF-8");
    public static final UUID NAMESPACE_MINECRAFT_DECENTRALIZED_AUTH = UUID.fromString("588672c8-7f77-43fc-98e7-0413caee61d4");
    /*public static final UUID NAMESPACE_DNS = UUID.fromString("6ba7b810-9dad-11d1-80b4-00c04fd430c8");
    public static final UUID NAMESPACE_URL = UUID.fromString("6ba7b811-9dad-11d1-80b4-00c04fd430c8");
    public static final UUID NAMESPACE_OID = UUID.fromString("6ba7b812-9dad-11d1-80b4-00c04fd430c8");
    public static final UUID NAMESPACE_X500 = UUID.fromString("6ba7b814-9dad-11d1-80b4-00c04fd430c8");*/

    public static UUID playerUUIDFromClientPubkey(Ed25519PublicKeyParameters pubkey) {
        return nameUUIDFromNamespaceAndBytes(NAMESPACE_MINECRAFT_DECENTRALIZED_AUTH, pubkey.getEncoded());
    }

    public static UUID nameUUIDFromNamespaceAndString(UUID namespace, String name) {
        return nameUUIDFromNamespaceAndBytes(namespace, Objects.requireNonNull(name, "name == null").getBytes(UTF8));
    }

    public static UUID nameUUIDFromNamespaceAndBytes(UUID namespace, byte[] name) {
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException nsae) {
            throw new InternalError("SHA-1 not supported");
        }
        md.update(toBytes(Objects.requireNonNull(namespace, "namespace is null")));
        md.update(Objects.requireNonNull(name, "name is null"));
        byte[] sha1Bytes = md.digest();
        sha1Bytes[6] &= 0x0f;  /* clear version        */
        sha1Bytes[6] |= 0x50;  /* set to version 5     */
        sha1Bytes[8] &= 0x3f;  /* clear variant        */
        sha1Bytes[8] |= 0x80;  /* set to IETF variant  */
        return fromBytes(sha1Bytes);
    }

    private static UUID fromBytes(byte[] data) {
        // Based on the private UUID(bytes[]) constructor
        long msb = 0;
        long lsb = 0;
        assert data.length >= 16;
        for (int i = 0; i < 8; i++)
            msb = (msb << 8) | (data[i] & 0xff);
        for (int i = 8; i < 16; i++)
            lsb = (lsb << 8) | (data[i] & 0xff);
        return new UUID(msb, lsb);
    }

    private static byte[] toBytes(UUID uuid) {
        // inverted logic of fromBytes()
        byte[] out = new byte[16];
        long msb = uuid.getMostSignificantBits();
        long lsb = uuid.getLeastSignificantBits();
        for (int i = 0; i < 8; i++)
            out[i] = (byte) ((msb >> ((7 - i) * 8)) & 0xff);
        for (int i = 8; i < 16; i++)
            out[i] = (byte) ((lsb >> ((15 - i) * 8)) & 0xff);
        return out;
    }
}
