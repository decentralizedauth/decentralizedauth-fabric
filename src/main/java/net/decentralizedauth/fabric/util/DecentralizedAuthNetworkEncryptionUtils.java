package net.decentralizedauth.fabric.util;

import java.security.InvalidAlgorithmParameterException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.KeyAgreement;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.bouncycastle.crypto.CryptoException;
import org.bouncycastle.crypto.Signer;
import org.bouncycastle.crypto.digests.Blake2bDigest;
import org.bouncycastle.crypto.generators.HKDFBytesGenerator;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.bouncycastle.crypto.params.HKDFParameters;
import org.bouncycastle.crypto.signers.Ed25519Signer;
import org.bouncycastle.jcajce.spec.XDHParameterSpec;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import net.minecraft.network.encryption.NetworkEncryptionException;
import net.minecraft.network.encryption.NetworkEncryptionUtils;

public class DecentralizedAuthNetworkEncryptionUtils {
  public static PublicKey decodeEncodedX25519PublicKey(byte[] publicKeyBytes) throws NetworkEncryptionException {
    KeyFactory keyFactory;
    try {
      keyFactory = KeyFactory.getInstance("X25519", BouncyCastleProvider.PROVIDER_NAME);
    } catch (NoSuchProviderException e) {
      throw new NetworkEncryptionException(e);
    } catch (NoSuchAlgorithmException e) {
      throw new NetworkEncryptionException(e);
    }
    try {
      return keyFactory.generatePublic(new X509EncodedKeySpec(publicKeyBytes));
    } catch (InvalidKeySpecException e) {
      throw new NetworkEncryptionException(e);
    }
  }

  public record AESCiphers(Cipher decryptCipher, Cipher encryptCipher) {}

  public static AESCiphers deriveAESCiphers(SecretKey sharedSecret) throws NetworkEncryptionException {
    HKDFBytesGenerator hkdfGenerator = new HKDFBytesGenerator(new Blake2bDigest());
    byte[] hkdfOutput = new byte[16];
    hkdfGenerator.init(HKDFParameters.defaultParameters(sharedSecret.getEncoded()));
    hkdfGenerator.generateBytes(hkdfOutput, 0, 16);
    SecretKey hkdfKey = new SecretKeySpec(hkdfOutput, "AES");

    Cipher decryptCipher = NetworkEncryptionUtils.cipherFromKey(Cipher.DECRYPT_MODE, hkdfKey);
    Cipher encryptCipher = NetworkEncryptionUtils.cipherFromKey(Cipher.ENCRYPT_MODE, hkdfKey);

    return new AESCiphers(decryptCipher, encryptCipher);
  }

  public static SecretKey finishX25519(PrivateKey privateKey, PublicKey remotePublicKey) throws NetworkEncryptionException {
    try {
      KeyAgreement keyAgreement = KeyAgreement.getInstance("X25519", BouncyCastleProvider.PROVIDER_NAME);
      keyAgreement.init(privateKey);
      keyAgreement.doPhase(remotePublicKey, true);
      return keyAgreement.generateSecret("X25519");
    } catch (Exception e) {
      throw new NetworkEncryptionException(e);
    }
  }

  public static KeyPair generateX25519EphemeralKeypair() {
    KeyPairGenerator generator;
    try {
      generator = KeyPairGenerator.getInstance("X25519", BouncyCastleProvider.PROVIDER_NAME);
    } catch (Exception e) {
      throw new IllegalStateException("Failed to generate network encryption key", e);
    }
    try {
      generator.initialize(new XDHParameterSpec(XDHParameterSpec.X25519));
    } catch (InvalidAlgorithmParameterException e) {
      throw new IllegalStateException("Failed to generate network encryption key", e);
    }
    return generator.generateKeyPair();
  }

  public static byte[] generateIdentityProof(Ed25519PrivateKeyParameters identityPrivateKey, SecretKey sharedSecret) {
    byte[] sharedSecretEncoded = sharedSecret.getEncoded();
    Signer signer = new Ed25519Signer();
    signer.init(true, identityPrivateKey);
    signer.update(sharedSecretEncoded, 0, sharedSecretEncoded.length);
    try {
      return signer.generateSignature();
    } catch (CryptoException e) {
      throw new IllegalStateException("Protocol error", e);
    }
  }

  public static boolean verifyIdentityProof(Ed25519PublicKeyParameters identityPublicKey, byte[] identityProof, SecretKey sharedSecret) {
    byte[] sharedSecretBytes = sharedSecret.getEncoded();
    Ed25519PublicKeyParameters serverIdentityPublicKey = identityPublicKey;
    Signer verifier = new Ed25519Signer();
    verifier.init(false, serverIdentityPublicKey);
    verifier.update(sharedSecretBytes, 0, sharedSecretBytes.length);
    return verifier.verifySignature(identityProof);
  }

  /**
   * Checking equality is a hard problem in computer science. `==` would make
   * too much sense. `.equals()` is common practice, but doesn't work on
   * `Ed25519PublicKeyParameters`. You'd also think `.equals()` would at least
   * work on the encoded version, but even that doesn't work.
   *
   * Excellent language design!
   */
  public static boolean sameKey(Ed25519PublicKeyParameters key1, Ed25519PublicKeyParameters key2) {
    return Arrays.equals(key1.getEncoded(), key2.getEncoded());
  }
}
