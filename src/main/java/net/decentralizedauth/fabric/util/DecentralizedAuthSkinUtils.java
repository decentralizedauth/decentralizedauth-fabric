package net.decentralizedauth.fabric.util;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.MemoryCacheImageInputStream;

import org.apache.commons.lang3.Validate;
import org.bouncycastle.util.encoders.Hex;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.properties.Property;
import com.mojang.authlib.yggdrasil.response.MinecraftTexturesPayload;
import com.mojang.util.UUIDTypeAdapter;

import ar.com.hjg.pngj.ImageInfo;
import ar.com.hjg.pngj.PngReader;
import net.decentralizedauth.fabric.access.MinecraftTexturesPayloadAccess;

public class DecentralizedAuthSkinUtils {
  private final static Gson gson = new GsonBuilder().registerTypeAdapter(UUID.class, new UUIDTypeAdapter()).create();

  // TODO handle slim model
  public static GameProfile assembleProfile(UUID id, String name, Map<MinecraftProfileTexture.Type, String> textureHashes) {
    Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> textures = new HashMap<MinecraftProfileTexture.Type, MinecraftProfileTexture>();

    for (Map.Entry<MinecraftProfileTexture.Type, String> textureHashEntry : textureHashes.entrySet()) {
      Map<String, String> textureMetadata = new HashMap<String, String>();
      textureMetadata.put("textureSha256", textureHashEntry.getValue());
      textures.put(textureHashEntry.getKey(), new MinecraftProfileTexture(null, textureMetadata));
    }

    GameProfile profile = new GameProfile(id, name);

    if (!textures.isEmpty()) {
      MinecraftTexturesPayload texturesPayload = new MinecraftTexturesPayload();
      ((MinecraftTexturesPayloadAccess) texturesPayload).setTimestamp(0L);
      ((MinecraftTexturesPayloadAccess) texturesPayload).setProfileId(id);
      ((MinecraftTexturesPayloadAccess) texturesPayload).setProfileName(name);
      ((MinecraftTexturesPayloadAccess) texturesPayload).setIsPublic(true);
      ((MinecraftTexturesPayloadAccess) texturesPayload).setTextures(textures);

      Property textureProp = new Property("textures", Base64.getEncoder().encodeToString(gson.toJson(texturesPayload).getBytes(StandardCharsets.UTF_8)));
      profile.getProperties().put("textures", textureProp);
    }

    return profile;
  }

  public static String sha256(byte[] textureData) {
    MessageDigest digest;
    try {
      digest = MessageDigest.getInstance("SHA-256");
    } catch (NoSuchAlgorithmException e) {
      throw new IllegalStateException("Protocol error", e);
    }
    final byte[] dataHash = digest.digest(textureData);
    return new String(Hex.encode(dataHash));
  }

  /**
   * Validates that the image is a PNG, as well as checking that the
   * `NativeImage` clientside code is able to load the image without any
   * issues.
   */
  public static void validateTexture(MinecraftProfileTexture.Type textureType, byte[] textureData) {
    // Note that image size is already limited to 16kB by packet parsing.
    Validate.validState(textureData.length < 16384);

    Iterator<ImageReader> pngReaders = ImageIO.getImageReadersByMIMEType("image/png");
    Validate.validState(pngReaders.hasNext(), "No PNG image reader available", new Object[0]);
    ImageReader r = pngReaders.next();
    Validate.validState(!pngReaders.hasNext(), "Ambiguous PNG readers detected", new Object[0]);
    ImageInputStream imageInputStream = new MemoryCacheImageInputStream(new ByteArrayInputStream(textureData));
    r.setInput(imageInputStream);
    ImageReadParam p = r.getDefaultReadParam();
    int numImages;
    try {
      numImages = r.getNumImages(true);
    } catch (Exception e) {
      throw new IllegalArgumentException("couldn't read number of images from PNG");
    }
    Validate.validState(numImages == 1, "too many images specified", new Object[0]);
    try {
      r.readAll(0, p);
    } catch(Exception e) {
      throw new IllegalArgumentException("Invalid PNG supplied for " + textureType.name() + " texture");
    }

    ByteArrayInputStream inputStream = new ByteArrayInputStream(textureData);
    PngReader pngr = new PngReader(inputStream);
    // TODO can we enforce that there is no EXIF metadata (to save size)?
    validateTexture(textureType, pngr);
  }

  static void validateTexture(MinecraftProfileTexture.Type type, PngReader image) {
    ImageInfo info = image.getImgInfo();
    switch (type) {
      case SKIN:
        Validate.validState(info.cols == 64, "'" + type.name() + "' texture has invalid width " + info.cols);
        Validate.validState(info.rows == 64 || info.rows == 32, "'" + type.name() + "' texture has invalid height " + info.rows);
        break;
      case CAPE:
        Validate.validState(info.cols == 64, "'" + type.name() + "' texture has invalid width " + info.cols);
        Validate.validState(info.rows == 32, "'" + type.name() + "' texture has invalid height " + info.rows);
        break;
      case ELYTRA:
        throw new IllegalArgumentException("ELYTRA textures are not valid");
    }
  }
}
