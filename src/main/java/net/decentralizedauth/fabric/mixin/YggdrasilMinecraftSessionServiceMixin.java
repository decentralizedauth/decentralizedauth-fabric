package net.decentralizedauth.fabric.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyVariable;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.mojang.authlib.yggdrasil.YggdrasilMinecraftSessionService;

@Mixin(YggdrasilMinecraftSessionService.class)
public class YggdrasilMinecraftSessionServiceMixin {
  @ModifyVariable(at = @At("HEAD"), argsOnly = true, method = "getTextures(Lcom/mojang/authlib/GameProfile;Z)Ljava/util/Map;", remap = false)
  public boolean onGetTextures(boolean requireSecure) {
    return false;
  }

  @Inject(at = @At("HEAD"), method = "isAllowedTextureDomain(Ljava/lang/String;)Z", cancellable = true, remap = false)
  private static void isAllowedTextureDomain(final String url, CallbackInfoReturnable<Boolean> info) {
    if (url == null) {
      info.setReturnValue(true);
      info.cancel();
    }
  }
}
