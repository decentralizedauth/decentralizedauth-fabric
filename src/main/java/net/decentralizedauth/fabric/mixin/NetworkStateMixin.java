package net.decentralizedauth.fabric.mixin;

import java.util.Map;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.ModifyConstant;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.decentralizedauth.fabric.client.ClientDecentralizedAuthPacketListener;
import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthChallengeS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthProofC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthCompressionS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthDisconnectS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthStartC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthSuccessS2CPacket;
import net.decentralizedauth.fabric.server.ServerDecentralizedAuthPacketListener;
import net.minecraft.network.NetworkSide;
import net.minecraft.network.NetworkState;
import net.minecraft.network.NetworkState.PacketHandler;
import net.minecraft.network.NetworkState.PacketHandlerInitializer;

@Mixin(NetworkState.class)
class NetworkStateMixin {
  @Shadow
  @Mutable
  private Map<NetworkSide, ? extends PacketHandler<?>> packetHandlers;

  @Shadow
  private static NetworkState[] STATES;

  @ModifyConstant(method = "<clinit>", constant = @Constant(intValue = 4), require = 1)
  private static int increaseNetworkStateArrayCapacity(int existingValue) {
    return 71;
  }

  // Modify only the `2` in the `if` condition for range checking. Ordinals 0
  // and 1 are "hidden" constants used in the constructors for enum variants.
  @ModifyConstant(method = "<clinit>", constant = @Constant(intValue = 2, ordinal = 2), require = 1)
  private static int increaseMaxNetworkStateValue(int existingValue) {
    return 69;
  }

  @Inject(at = @At("RETURN"), method = "<init>")
  private void constructorMixin(String variantName, int id1, int id2, PacketHandlerInitializer initializer, CallbackInfo info) {
    if (variantName == "DECENTRALIZEDAUTH") {
      initializer.setup(
          NetworkSide.SERVERBOUND,
          new NetworkState.PacketHandler<ServerDecentralizedAuthPacketListener>()
            .register(DecentralizedAuthStartC2SPacket.class, DecentralizedAuthStartC2SPacket::new)
            .register(DecentralizedAuthEncryptionResponseC2SPacket.class, DecentralizedAuthEncryptionResponseC2SPacket::new)
            .register(DecentralizedAuthAuthProofC2SPacket.class, DecentralizedAuthAuthProofC2SPacket::new)
            .register(DecentralizedAuthProfileResponseC2SPacket.class, DecentralizedAuthProfileResponseC2SPacket::new))
        .setup(
          NetworkSide.CLIENTBOUND,
          new NetworkState.PacketHandler<ClientDecentralizedAuthPacketListener>()
            .register(DecentralizedAuthDisconnectS2CPacket.class, DecentralizedAuthDisconnectS2CPacket::new)
            .register(DecentralizedAuthEncryptionRequestS2CPacket.class, DecentralizedAuthEncryptionRequestS2CPacket::new)
            .register(DecentralizedAuthAuthChallengeS2CPacket.class, DecentralizedAuthAuthChallengeS2CPacket::new)
            .register(DecentralizedAuthProfileRequestS2CPacket.class, DecentralizedAuthProfileRequestS2CPacket::new)
            .register(DecentralizedAuthCompressionS2CPacket.class, DecentralizedAuthCompressionS2CPacket::new)
            .register(DecentralizedAuthSuccessS2CPacket.class, DecentralizedAuthSuccessS2CPacket::new));
      this.packetHandlers = initializer.packetHandlers;
    }
  }

  @Inject(at = @At("HEAD"), method = "byId(I)Lnet/minecraft/network/NetworkState;", cancellable = true)
  private static void onById(int id, CallbackInfoReturnable<NetworkState> info) {
    if (id == 69) {
      info.setReturnValue(STATES[id - -1]);
      info.cancel();
    }
  }
}
