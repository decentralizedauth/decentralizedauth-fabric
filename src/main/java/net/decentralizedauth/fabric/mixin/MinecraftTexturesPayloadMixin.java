package net.decentralizedauth.fabric.mixin;

import java.util.Map;
import java.util.UUID;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.yggdrasil.response.MinecraftTexturesPayload;

import net.decentralizedauth.fabric.access.MinecraftTexturesPayloadAccess;

@Mixin(MinecraftTexturesPayload.class)
public class MinecraftTexturesPayloadMixin implements MinecraftTexturesPayloadAccess {
  @Shadow(remap = false)
  @Mutable
  private long timestamp;
  @Shadow(remap = false)
  @Mutable
  private UUID profileId;
  @Shadow(remap = false)
  @Mutable
  private String profileName;
  @Shadow(remap = false)
  @Mutable
  private boolean isPublic;
  @Shadow(remap = false)
  @Mutable
  private Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> textures;

  @Override
  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  @Override
  public void setProfileId(UUID profileId) {
    this.profileId = profileId;
  }

  @Override
  public void setProfileName(String profileName) {
    this.profileName = profileName;
  }

  @Override
  public void setIsPublic(boolean isPublic) {
    this.isPublic = isPublic;
  }

  @Override
  public void setTextures(Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> textures) {
    this.textures = textures;
  }
}
