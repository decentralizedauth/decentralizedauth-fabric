package net.decentralizedauth.fabric.mixin.server;

import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.GameProfileRepository;

import net.decentralizedauth.fabric.access.UserCacheAccess;
import net.minecraft.util.UserCache;

@Mixin(UserCache.class)
public abstract class UserCacheMixin implements UserCacheAccess {
  @Shadow
  private Map<String, UserCache.Entry> byName;
  @Shadow
  private Map<UUID, UserCache.Entry> byUuid;
  @Shadow
  private GameProfileRepository profileRepository;

  @Shadow
  abstract void save();

  /**
   * Looks up a user by name in the local cache only, without changing the
   * profile's last access time if the profile exists in the cache.
   */
  @Override
  public Optional<GameProfile> findCachedByName(String name) {
    Optional<GameProfile> optional;
    String string = name.toLowerCase(Locale.ROOT);
    UserCache.Entry entry = this.byName.get(string);
    boolean cacheModified = false;
    if (entry != null && new Date().getTime() >= entry.expirationDate.getTime()) {
      this.byUuid.remove(entry.getProfile().getId());
      this.byName.remove(entry.getProfile().getName().toLowerCase(Locale.ROOT));
      cacheModified = true;
      entry = null;
    }
    if (entry != null) {
      optional = Optional.of(entry.getProfile());
    } else {
      optional = Optional.empty();
    }
    if (cacheModified) {
      this.save();
    }
    return optional;
  }

  /**
   * Looks up a user by name from Mojang's services, without adding it to the
   * cache or incrementing any last access times of cached accounts.
   */
  @Override
  public Optional<GameProfile> findOfficialAccountByName(String name) {
    Optional<GameProfile> optional;
    String string = name.toLowerCase(Locale.ROOT);
    return UserCache.findProfileByName(this.profileRepository, string);
  }

  @Override
  public void removeByUUID(UUID id) {
    UserCache.Entry entry = this.byUuid.get(id);
    if (entry != null) {
      this.byUuid.remove(id);
      this.byName.remove(entry.getProfile().getName().toLowerCase(Locale.ROOT));
    }
  }
}
