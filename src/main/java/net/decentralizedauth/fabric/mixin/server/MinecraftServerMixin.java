package net.decentralizedauth.fabric.mixin.server;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.security.SecureRandom;
import java.util.Optional;

import javax.annotation.Nullable;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.MinecraftServerAccess;
import net.decentralizedauth.fabric.access.ServerMetadataAccess;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.ServerMetadata;

@Mixin(MinecraftServer.class)
public abstract class MinecraftServerMixin implements MinecraftServerAccess {
  @Shadow
  public abstract File getFile(String path);

  @Shadow
  private ServerMetadata metadata;

  @Shadow
  public abstract File getRunDirectory();

  @Nullable
  @Unique
  private Ed25519PrivateKeyParameters id;

  @Unique
  private File decentralizedAuthServerDirectory;

  @Override
  public void setDecentralizedAuthIdentity(Ed25519PrivateKeyParameters id) {
    this.id = id;
  }

  @Override
  @Nullable
  public Ed25519PrivateKeyParameters getDecentralizedAuthIdentity() {
    return this.id;
  }

  @Override
  public File getDecentralizedAuthClientDirectory() {
    return this.decentralizedAuthServerDirectory;
  }

  @Override
  public File getServerTextureCachePath(MinecraftProfileTexture.Type type, String textureSha256) {
    String hashPrefix = textureSha256.length() > 2 ? textureSha256.substring(0, 2) : "xx";
    Path cacheFilePath = this.decentralizedAuthServerDirectory.toPath().resolve("textureCache").resolve(type.name()).resolve(hashPrefix).resolve(textureSha256);
    return cacheFilePath.toFile();
  }

  private void initializeDecentralizedAuthIdentity(ServerMetadata metadata) {
    File identityFile = new File(this.decentralizedAuthServerDirectory, "identitySecretKey");
    Optional<File> optional_keypair_path = Optional.of(identityFile).filter(File::isFile);
    Optional<Ed25519PrivateKeyParameters> optional_keypair = optional_keypair_path.map(file -> {
      try {
        byte[] keyData = Files.readAllBytes(file.toPath());
        Ed25519PrivateKeyParameters privateKey = new Ed25519PrivateKeyParameters(keyData);
        return privateKey;
      } catch (IOException e) {
        DecentralizedAuth.LOGGER.info(e.getMessage());
      }
      return null;
    });
    if (!optional_keypair.isPresent()) {
      DecentralizedAuth.LOGGER.info("No usable Decentralized Auth identity found, generating a new keypair for you!");
      SecureRandom random = new SecureRandom();
      Ed25519PrivateKeyParameters pk = new Ed25519PrivateKeyParameters(random);
      byte[] keyData = pk.getEncoded();
      try {
        Files.write(identityFile.toPath(), keyData);
      } catch (IOException e) {
        DecentralizedAuth.LOGGER.info(e.getMessage());
      }
      DecentralizedAuth.LOGGER.info("Your new Decentralized Auth identity was written to `identitySecretKey`.");
      DecentralizedAuth.LOGGER.info("Don't share this file with anyone else!");
      optional_keypair = Optional.of(pk);
    }

    ((ServerMetadataAccess) this.metadata).setDecentralizedAuthSupport(1);
    this.setDecentralizedAuthIdentity(optional_keypair.get());
    DecentralizedAuth.LOGGER.info("Decentralized Auth is ready to accept new connections");
  }

  @Inject(at = @At(value = "INVOKE", target = "net/minecraft/server/MinecraftServer.setFavicon(Lnet/minecraft/server/ServerMetadata;)V"), method = "runServer()V")
  protected void runServerMixin(CallbackInfo info) {
    Path serverModDirectory = this.getRunDirectory().toPath().resolve("decentralizedauth").resolve("server");
    try {
      Files.createDirectories(serverModDirectory, new FileAttribute[0]);
    } catch (Exception e) {
      DecentralizedAuth.LOGGER.error("Failed to create Decentralized Auth server directory.");
      System.exit(-1);
    }
    this.decentralizedAuthServerDirectory = serverModDirectory.toFile();
    DecentralizedAuth.LOGGER.info("Loading Decentralized Auth identity...");
    this.initializeDecentralizedAuthIdentity(this.metadata);
  }
}
