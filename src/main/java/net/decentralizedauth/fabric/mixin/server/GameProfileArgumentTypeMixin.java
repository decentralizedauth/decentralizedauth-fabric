package net.decentralizedauth.fabric.mixin.server;

import java.util.Collections;
import java.util.UUID;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import com.mojang.authlib.GameProfile;
import com.mojang.brigadier.StringReader;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;

import net.minecraft.command.argument.GameProfileArgumentType;
import net.minecraft.command.argument.GameProfileArgumentType.GameProfileArgument;
import net.minecraft.text.Text;

@Mixin(GameProfileArgumentType.class)
public class GameProfileArgumentTypeMixin {
  @Unique
  private static final SimpleCommandExceptionType INVALID_UUID_EXCEPTION = new SimpleCommandExceptionType(Text.of("Invalid UUID"));

  @Inject(at = @At(value = "RETURN", ordinal = 1), method = "parse(Lcom/mojang/brigadier/StringReader;)Lnet/minecraft/command/argument/GameProfileArgumentType$GameProfileArgument;", locals = LocalCapture.CAPTURE_FAILHARD, cancellable = true)
  public void onParse(StringReader stringReader, CallbackInfoReturnable<GameProfileArgument> info, int i, String string) {
    if (string.length() == 36) {
      info.setReturnValue(source -> {
        UUID id;
        try {
          id = UUID.fromString(string);
        } catch(IllegalArgumentException e) {
          throw INVALID_UUID_EXCEPTION.create();
        }
        return Collections.singleton(new GameProfile(id, string));
      });
      info.cancel();
    }
  }
}
