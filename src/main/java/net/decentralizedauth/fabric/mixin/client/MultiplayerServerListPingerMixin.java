package net.decentralizedauth.fabric.mixin.client;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.decentralizedauth.fabric.access.ServerInfoAccess;
import net.decentralizedauth.fabric.access.ServerMetadataAccess;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.network.packet.s2c.query.QueryResponseS2CPacket;
import net.minecraft.server.ServerMetadata;

// This is the anonymous ClientQueryPacketListener class
@Mixin(targets = {"net/minecraft/client/network/MultiplayerServerListPinger$1"})
public class MultiplayerServerListPingerMixin {
  // Captured `entry` argument
  @Shadow
  ServerInfo field_3776;

  @Inject(at = @At("RETURN"), method = "onResponse")
  public void onOnResponse(QueryResponseS2CPacket packet, CallbackInfo info) {
    ServerMetadata serverMetadata = packet.getServerMetadata();
    Integer decentralizedAuthVersion = ((ServerMetadataAccess) serverMetadata).getDecentralizedAuthSupport();
    if (decentralizedAuthVersion != null && decentralizedAuthVersion == 1) {
      ServerInfo entry = this.field_3776;
      ((ServerInfoAccess) entry).setDecentralizedAuthSupport(true);
    }
  }
}
