package net.decentralizedauth.fabric.mixin.client;

import java.net.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.google.common.collect.ImmutableList;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.datafixers.DataFixer;

import net.decentralizedauth.fabric.access.MinecraftClientAccess;
import net.decentralizedauth.fabric.util.DecentralizedAuthSkinUtils;
import net.decentralizedauth.fabric.util.UUIDv5;
import net.minecraft.client.MinecraftClient;
import net.minecraft.resource.ResourcePackManager;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.SaveLoader;
import net.minecraft.server.WorldGenerationProgressListenerFactory;
import net.minecraft.server.integrated.IntegratedServer;
import net.minecraft.util.ApiServices;
import net.minecraft.world.level.storage.LevelStorage;
import net.minecraft.world.level.storage.LevelStorage.Session;

@Mixin(IntegratedServer.class)
public abstract class IntegratedServerMixin extends MinecraftServer {
  public IntegratedServerMixin(Thread serverThread, Session session, ResourcePackManager dataPackManager, SaveLoader saveLoader, Proxy proxy, DataFixer dataFixer, ApiServices apiServices, WorldGenerationProgressListenerFactory worldGenerationProgressListenerFactory) {
    super(serverThread, session, dataPackManager, saveLoader, proxy, dataFixer, apiServices, worldGenerationProgressListenerFactory);
    throw new IllegalStateException("IntegratedServerMixin should not be constructed directly");
  }

  @Inject(at = @At("RETURN"), method = "<init>")
  public void onConstructor(Thread serverThread, MinecraftClient client, LevelStorage.Session session, ResourcePackManager dataPackManager, SaveLoader saveLoader, ApiServices apiServices, WorldGenerationProgressListenerFactory worldGenerationProgressListenerFactory, CallbackInfo info) {
    Ed25519PrivateKeyParameters privateKey = ((MinecraftClientAccess) client).getDecentralizedAuthIdentity();
    Ed25519PublicKeyParameters publicKey = privateKey.generatePublicKey();
    UUID uuid = UUIDv5.playerUUIDFromClientPubkey(publicKey);

    Map<MinecraftProfileTexture.Type, String> textureHashes = new HashMap<MinecraftProfileTexture.Type, String>();
    Map<MinecraftProfileTexture.Type, byte[]> textureData = new HashMap<MinecraftProfileTexture.Type, byte[]>();
    ImmutableList.of(MinecraftProfileTexture.Type.SKIN, MinecraftProfileTexture.Type.CAPE).forEach(type -> {
      ((MinecraftClientAccess) client).prepareTextureData(type, Optional.empty(), textureHashes, textureData);
    });

    GameProfile hostProfile = DecentralizedAuthSkinUtils.assembleProfile(uuid, client.getSession().getUsername(), textureHashes);

    this.setHostProfile(hostProfile);
  }
}
