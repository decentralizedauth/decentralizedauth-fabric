package net.decentralizedauth.fabric.mixin.client;

import org.spongepowered.asm.mixin.Mixin;

import net.decentralizedauth.fabric.access.ServerInfoAccess;
import net.minecraft.client.network.ServerInfo;

@Mixin(ServerInfo.class)
public class ServerInfoMixin implements ServerInfoAccess {
  private boolean decentralizedAuthSupport = false;

  @Override
  public void setDecentralizedAuthSupport(boolean supported) {
    this.decentralizedAuthSupport = supported;
  }

  @Override
  public boolean getDecentralizedAuthSupport() {
    return this.decentralizedAuthSupport;
  }
}
