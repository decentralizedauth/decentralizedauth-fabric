package net.decentralizedauth.fabric.mixin.client;

import java.util.Collections;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.mojang.blaze3d.systems.RenderSystem;

import net.decentralizedauth.fabric.access.ServerInfoAccess;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerScreen;
import net.minecraft.client.gui.screen.multiplayer.MultiplayerServerListWidget.ServerEntry;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

@Mixin(ServerEntry.class)
public class ServerEntryMixin {
  @Shadow
  private MultiplayerScreen screen;
  @Shadow
  private MinecraftClient client;
  @Shadow
  private ServerInfo server;

  @Inject(at = @At("RETURN"), method = "render")
  public void onRender(MatrixStack matrices, int index, int y, int x, int entryWidth, int entryHeight, int mouseX, int mouseY, boolean hovered, float tickDelta, CallbackInfo info) {
    boolean decentralizedAuthSupported = ((ServerInfoAccess) this.server).getDecentralizedAuthSupport();
    if (decentralizedAuthSupported) {
      RenderSystem.setShader(GameRenderer::getPositionTexShader);
      RenderSystem.setShaderColor(1.0f, 1.0f, 1.0f, 1.0f);
      RenderSystem.setShaderTexture(0, new Identifier("decentralizedauth", "icon.png"));
      RenderSystem.enableBlend();
      RenderSystem.defaultBlendFunc();
      DrawableHelper.drawTexture(matrices, x + entryWidth - 16 - 4, y + 12, 0, 0, 16, 16, 16, 16);

      int m = mouseX - x;
      int n = mouseY - y;
      if (m >= entryWidth - 16 - 4 && m <= entryWidth - 4 && n >= 12 && n <= 12 + 16) {
        this.screen.setTooltip(Collections.singletonList(Text.of("Decentralized Auth is supported")));
      }
    }
  }
}
