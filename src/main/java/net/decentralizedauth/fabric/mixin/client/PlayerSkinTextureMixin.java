package net.decentralizedauth.fabric.mixin.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.CompletableFuture;

import net.minecraft.util.Util;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.PlayerSkinProviderAccess;
import net.decentralizedauth.fabric.access.PlayerSkinTextureAccess;
import net.decentralizedauth.fabric.packet.PlayTextureRequestC2SPacket;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.texture.PlayerSkinTexture;
import net.minecraft.network.PacketByteBuf;

@Mixin(PlayerSkinTexture.class)
public abstract class PlayerSkinTextureMixin implements PlayerSkinTextureAccess {
  @Shadow
  @Mutable
  private String url;
  @Shadow
  private File cacheFile;
  @Shadow
  protected abstract void onTextureLoaded(NativeImage image);
  @Shadow
  protected abstract NativeImage loadTexture(InputStream stream);

  @Shadow
  private @Nullable CompletableFuture<?> loader;

  private String sha256;

  private PlayerSkinProviderAccess skinProvider;

  @Inject(method = "load", at = @At(value = "INVOKE", target = "java/util/concurrent/CompletableFuture.runAsync(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)Ljava/util/concurrent/CompletableFuture;"), cancellable = true)
  private void onLoad(CallbackInfo info) {
    // with this assignment, we supply our own skin loader if:
    // - there was no loader assigned yet
    // - there was no image found in cache
    loader = CompletableFuture.runAsync(() -> {
      if (this.url == null && this.sha256 != null) {
        skinProvider.registerPendingTextureRequest(sha256, this);

        PacketByteBuf requestPacketBuf = PacketByteBufs.create();
        new PlayTextureRequestC2SPacket(sha256).write(requestPacketBuf);
        ClientPlayNetworking.send(DecentralizedAuth.TEXTURES_CHANNEL, requestPacketBuf);
      }
    }, Util.getMainWorkerExecutor());
    info.cancel();
  }

  @Override
  public void setTextureSha256(String sha256) {
    this.sha256 = sha256;
  }

  @Override
  public void setPlayerSkinProvider(PlayerSkinProviderAccess provider) {
    this.skinProvider = provider;
  }

  @Override
  public void onTextureLoadedFromServer(byte[] textureData) {
    try {
      InputStream inputStream;
      if (this.cacheFile != null) {
        FileUtils.copyInputStreamToFile(new ByteArrayInputStream(textureData), this.cacheFile);
        inputStream = new FileInputStream(this.cacheFile);
      } else {
        inputStream = new ByteArrayInputStream(textureData);
      }
      MinecraftClient.getInstance().execute(() -> {
        NativeImage nativeImage = this.loadTexture(inputStream);
        if (nativeImage != null) {
          this.onTextureLoaded(nativeImage);
        }
      });
    } catch(Exception exception) {
      DecentralizedAuth.LOGGER.error("Couldn't save server-sent texture", exception);
    }
  }
}
