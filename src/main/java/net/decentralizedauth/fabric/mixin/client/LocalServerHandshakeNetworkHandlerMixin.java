package net.decentralizedauth.fabric.mixin.client;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.chocohead.mm.api.ClassTinkerers;

import net.decentralizedauth.fabric.server.ServerDecentralizedAuthNetworkHandler;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.NetworkState;
import net.minecraft.network.packet.c2s.handshake.HandshakeC2SPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.LocalServerHandshakeNetworkHandler;

@Mixin(LocalServerHandshakeNetworkHandler.class)
public class LocalServerHandshakeNetworkHandlerMixin {
  @Shadow
  private MinecraftServer server;
  @Shadow
  private ClientConnection connection;

  @Inject(at = @At("HEAD"), method = "onHandshake(Lnet/minecraft/network/packet/c2s/handshake/HandshakeC2SPacket;)V", cancellable = true)
  public void onOnHandshake(HandshakeC2SPacket packet, CallbackInfo info) {
    NetworkState decentralizedAuth = ClassTinkerers.getEnum(NetworkState.class, "DECENTRALIZEDAUTH");
    if (packet.getIntendedState() == decentralizedAuth) {
      this.connection.setState(decentralizedAuth);
      this.connection.setPacketListener(new ServerDecentralizedAuthNetworkHandler(this.server, this.connection));
      info.cancel();
    }
  }
}
