package net.decentralizedauth.fabric.mixin.client;

import java.io.File;
import java.util.HashMap;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.PlayerSkinProviderAccess;
import net.decentralizedauth.fabric.access.PlayerSkinTextureAccess;
import net.decentralizedauth.fabric.util.DecentralizedAuthSkinUtils;
import net.minecraft.client.texture.AbstractTexture;
import net.minecraft.client.texture.PlayerSkinProvider;
import net.minecraft.client.texture.PlayerSkinProvider.SkinTextureAvailableCallback;
import net.minecraft.client.texture.PlayerSkinTexture;
import net.minecraft.util.Identifier;

@Mixin(PlayerSkinProvider.class)
public class PlayerSkinProviderMixin implements PlayerSkinProviderAccess {
  private HashMap<String, PlayerSkinTextureAccess> pendingTextureRequests = new HashMap<String, PlayerSkinTextureAccess>();

  @Inject(at = @At(value = "INVOKE", target = "net/minecraft/client/texture/TextureManager.registerTexture(Lnet/minecraft/util/Identifier;Lnet/minecraft/client/texture/AbstractTexture;)V"), method = "loadSkin(Lcom/mojang/authlib/minecraft/MinecraftProfileTexture;Lcom/mojang/authlib/minecraft/MinecraftProfileTexture$Type;Lnet/minecraft/client/texture/PlayerSkinProvider$SkinTextureAvailableCallback;)Lnet/minecraft/util/Identifier;", locals = LocalCapture.CAPTURE_FAILEXCEPTION)
  private void onLoadSkin(MinecraftProfileTexture profileTexture, MinecraftProfileTexture.Type type, SkinTextureAvailableCallback callback, CallbackInfoReturnable<Identifier> info, String s, Identifier i, AbstractTexture a, File f, File f2, PlayerSkinTexture playerSkinTexture) {
    ((PlayerSkinTextureAccess) playerSkinTexture).setTextureSha256(profileTexture.getHash());
    ((PlayerSkinTextureAccess) playerSkinTexture).setPlayerSkinProvider(this);
  }

  @Override
  public void registerPendingTextureRequest(String textureSha256, PlayerSkinTextureAccess pendingTexture) {
    if (this.pendingTextureRequests.containsKey(textureSha256)) {
      // TODO: unclear if this case is possible or not.
      DecentralizedAuth.LOGGER.error("Multiple pending texture requests for a single texture SHA256 hash. Please report this error upstream.");
      throw new IllegalStateException("Multiple pending texture requests for a single texture SHA256 hash. Please report this error upstream.");
    }
    this.pendingTextureRequests.put(textureSha256, pendingTexture);
  }

  @Override
  public void onReceiveTextureFromServer(byte[] textureData) {
    String textureSha256 = DecentralizedAuthSkinUtils.sha256(textureData);
    if (this.pendingTextureRequests.containsKey(textureSha256)) {
      PlayerSkinTextureAccess pendingTexture = this.pendingTextureRequests.get(textureSha256);
      ((PlayerSkinTextureAccess) pendingTexture).onTextureLoadedFromServer(textureData);
      this.pendingTextureRequests.remove(textureSha256);
    }
  }
}
