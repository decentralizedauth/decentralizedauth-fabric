package net.decentralizedauth.fabric.mixin.client;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.SocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.FileAttribute;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Mutable;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import com.chocohead.mm.api.ClassTinkerers;
import com.google.common.collect.ImmutableList;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.blaze3d.systems.RenderSystem;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.MinecraftClientAccess;
import net.decentralizedauth.fabric.client.ClientDecentralizedAuthNetworkHandler;
import net.decentralizedauth.fabric.client.DecentralizedAuthKnownServers;
import net.decentralizedauth.fabric.packet.DecentralizedAuthStartC2SPacket;
import net.decentralizedauth.fabric.util.DecentralizedAuthSkinUtils;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.RunArgs;
import net.minecraft.client.gui.screen.LevelLoadingScreen;
import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.texture.NativeImage;
import net.minecraft.client.texture.NativeImageBackedTexture;
import net.minecraft.client.texture.PlayerSkinTexture;
import net.minecraft.client.texture.TextureManager;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.NetworkState;
import net.minecraft.network.packet.c2s.handshake.HandshakeC2SPacket;
import net.minecraft.resource.ResourcePackManager;
import net.minecraft.server.SaveLoader;
import net.minecraft.util.Identifier;
import net.minecraft.world.level.storage.LevelStorage;

@Mixin(MinecraftClient.class)
public abstract class MinecraftClientMixin implements MinecraftClientAccess {
  @Shadow
  public File runDirectory;

  @Shadow
  public abstract MinecraftSessionService getSessionService();

  @Shadow
  public abstract TextureManager getTextureManager();

  @Shadow
  @Mutable
  private ClientConnection integratedServerConnection;

  @Nullable
  @Unique
  private Ed25519PrivateKeyParameters id;

  @Unique
  private DecentralizedAuthKnownServers knownServers;

  @Unique
  private File decentralizedAuthClientDirectory;

  @Override
  public void setDecentralizedAuthIdentity(Ed25519PrivateKeyParameters id) {
    this.id = id;
  }

  @Override
  @Nullable
  public Ed25519PrivateKeyParameters getDecentralizedAuthIdentity() {
    return this.id;
  }

  @Override
  public File getDecentralizedAuthClientDirectory() {
    return this.decentralizedAuthClientDirectory;
  }

  @Override
  public DecentralizedAuthKnownServers getDecentralizedAuthKnownServers() {
    return this.knownServers;
  }

  private void initializeDecentralizedAuthIdentity() {
    this.knownServers = new DecentralizedAuthKnownServers((MinecraftClient) (Object) this);

    File identityFile = new File(this.decentralizedAuthClientDirectory, "identitySecretKey");
    Optional<File> optional_keypair_path = Optional.of(identityFile).filter(File::isFile);
    Optional<Ed25519PrivateKeyParameters> optional_keypair = optional_keypair_path.map(file -> {
      try {
        byte[] keyData = Files.readAllBytes(file.toPath());
        Ed25519PrivateKeyParameters privateKey = new Ed25519PrivateKeyParameters(keyData);
        return privateKey;
      } catch (IOException e) {
        DecentralizedAuth.LOGGER.info(e.getMessage());
      }
      return null;
    });
    if (!optional_keypair.isPresent()) {
      DecentralizedAuth.LOGGER.info("No usable Decentralized Auth identity found, generating a new keypair for you!");
      SecureRandom random = new SecureRandom();
      Ed25519PrivateKeyParameters pk = new Ed25519PrivateKeyParameters(random);
      byte[] keyData = pk.getEncoded();
      try {
        Files.write(identityFile.toPath(), keyData);
      } catch (IOException e) {
        DecentralizedAuth.LOGGER.info(e.getMessage());
      }
      DecentralizedAuth.LOGGER.info("Your new Decentralized Auth identity was written to `identitySecretKey`.");
      DecentralizedAuth.LOGGER.info("Don't share this file with anyone else!");
      optional_keypair = Optional.of(pk);
    }

    this.setDecentralizedAuthIdentity(optional_keypair.get());
    DecentralizedAuth.LOGGER.info("Decentralized Auth is ready to connect");
  }

  private void preloadPlayerTextureData() {
    Map<MinecraftProfileTexture.Type, String> textureHashes = new HashMap<MinecraftProfileTexture.Type, String>();
    Map<MinecraftProfileTexture.Type, byte[]> textureData = new HashMap<MinecraftProfileTexture.Type, byte[]>();

    ImmutableList.of(MinecraftProfileTexture.Type.SKIN, MinecraftProfileTexture.Type.CAPE).forEach(type -> {
      this.prepareTextureData(type, Optional.empty(), textureHashes, textureData);
      if (textureHashes.containsKey(type) && textureData.containsKey(type)) {
        this.preloadPlayerTextureData(type, textureHashes.get(type), textureData.get(type));
      }
    });
  }

  private void preloadPlayerTextureData(MinecraftProfileTexture.Type type, String textureSha256, byte[] textureData) {
    ByteArrayInputStream skinData = new ByteArrayInputStream(textureData);
    Identifier skinId = AbstractClientPlayerEntity.getSkinId(textureSha256);
    RenderSystem.recordRenderCall(() -> {
      NativeImage skinImage;
      try {
        skinImage = NativeImage.read(skinData);

        if (type == MinecraftProfileTexture.Type.SKIN) {
          PlayerSkinTexture.stripAlpha(skinImage, 0, 0, 32, 16);
          PlayerSkinTexture.stripAlpha(skinImage, 0, 16, 64, 32);
          PlayerSkinTexture.stripAlpha(skinImage, 16, 48, 48, 64);
        }

        NativeImageBackedTexture texture = new NativeImageBackedTexture(skinImage);

        if (this.getTextureManager().getOrDefault(skinId, null) == null) {
          this.getTextureManager().registerTexture(skinId, texture);
        }
      } catch (IOException e) {
      }
    });
  }

  @Inject(at = @At(value = "INVOKE", target = "net/minecraft/client/MinecraftClient.setOverlay(Lnet/minecraft/client/gui/screen/Overlay;)V"), method = "<init>(Lnet/minecraft/client/RunArgs;)V")
  protected void runServerMixin(RunArgs args, CallbackInfo info) {
    Path clientModDirectory = this.runDirectory.toPath().resolve("decentralizedauth").resolve("client");
    try {
      Files.createDirectories(clientModDirectory, new FileAttribute[0]);
    } catch (Exception e) {
      DecentralizedAuth.LOGGER.error("Failed to create Decentralized Auth client directory.");
      System.exit(-1);
    }
    this.decentralizedAuthClientDirectory = clientModDirectory.toFile();
    DecentralizedAuth.LOGGER.info("Loading Decentralized Auth identity...");
    this.initializeDecentralizedAuthIdentity();
    this.preloadPlayerTextureData();
  }

  @Inject(at = @At(value = "INVOKE", target = "net/minecraft/network/ClientConnection.setPacketListener(Lnet/minecraft/network/listener/PacketListener;)V"), method = "startIntegratedServer", locals = LocalCapture.CAPTURE_FAILHARD, cancellable = true)
  protected void startIntegratedServerMixin(String levelName, LevelStorage.Session session, ResourcePackManager dataPackManager, SaveLoader saveLoader, CallbackInfo info, LevelLoadingScreen levelLoadingScreen, SocketAddress socketAddress, ClientConnection clientConnection) {
    NetworkState decentralizedAuth = ClassTinkerers.getEnum(NetworkState.class, "DECENTRALIZEDAUTH");

    clientConnection.setPacketListener(new ClientDecentralizedAuthNetworkHandler(null, clientConnection, (MinecraftClient) (Object) this, null, status -> {}));
    clientConnection.send(new HandshakeC2SPacket(socketAddress.toString(), 0, decentralizedAuth));
    clientConnection.send(new DecentralizedAuthStartC2SPacket());
    this.integratedServerConnection = clientConnection;
    info.cancel();
  }

  /**
   * Loads a texture from disk and puts its hash into `textureHashes`, as well
   * as putting the texture itself into `newTextureData` if the server does not
   * appear to have the texture cached already.
   */
  @Override
  public void prepareTextureData(MinecraftProfileTexture.Type textureType, Optional<GameProfile> cachedIdentity, Map<MinecraftProfileTexture.Type, String> textureHashes, Map<MinecraftProfileTexture.Type, byte[]> newTextureData) {
    File textureFile = new File(this.decentralizedAuthClientDirectory, textureType.name() + ".png");

    if (!textureFile.isFile()) {
      return;
    }

    byte[] textureData;
    try {
      textureData = Files.readAllBytes(textureFile.toPath());
    } catch (IOException e) {
      DecentralizedAuth.LOGGER.info("failed to read " + textureFile.getPath() + ": ", e.getMessage());
      return;
    }

    final String textureSha256 = DecentralizedAuthSkinUtils.sha256(textureData);
    textureHashes.put(textureType, textureSha256);

    boolean skipSendingTexture = false;

    if (cachedIdentity.isPresent()) {
      Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> cachedTextures = this.getSessionService().getTextures(cachedIdentity.get(), false);
      if (cachedTextures.containsKey(textureType)) {
        MinecraftProfileTexture cachedTexture = cachedTextures.get(textureType);
        String cachedSha256 = cachedTexture.getMetadata("textureSha256");
        if (cachedSha256.equals(textureSha256)) {
          skipSendingTexture = true;
        }
      }
    }

    if (!skipSendingTexture) {
      newTextureData.put(textureType, textureData);
    }
  }
}
