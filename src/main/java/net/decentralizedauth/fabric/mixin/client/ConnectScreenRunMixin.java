package net.decentralizedauth.fabric.mixin.client;

import java.net.InetSocketAddress;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import com.chocohead.mm.api.ClassTinkerers;

import net.decentralizedauth.fabric.access.ServerInfoAccess;
import net.decentralizedauth.fabric.client.ClientDecentralizedAuthNetworkHandler;
import net.decentralizedauth.fabric.packet.DecentralizedAuthStartC2SPacket;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ConnectScreen;
import net.minecraft.client.network.ServerAddress;
import net.minecraft.client.network.ServerInfo;
import net.minecraft.network.NetworkState;
import net.minecraft.network.packet.c2s.handshake.HandshakeC2SPacket;

@Mixin(targets = {"net/minecraft/client/gui/screen/ConnectScreen$1"})
class ConnectScreenRunMixin {
  // screen
  @Shadow
  ConnectScreen field_2416;
  // address
  @Shadow
  ServerAddress field_33737;
  // client
  @Shadow
  MinecraftClient field_33738;

  @Inject(at = @At(value = "INVOKE", target = "net/minecraft/network/ClientConnection.setPacketListener(Lnet/minecraft/network/listener/PacketListener;)V"), method = "run()V", locals = LocalCapture.CAPTURE_FAILHARD, cancellable = true)
  public void onRun(CallbackInfo info, InetSocketAddress inetSocketAddress) {
    ConnectScreen screen = this.field_2416;
    ServerAddress address = this.field_33737;
    MinecraftClient client = this.field_33738;
    ServerInfo serverInfo = client.getCurrentServerEntry();
    if (((ServerInfoAccess) serverInfo).getDecentralizedAuthSupport()) {
      NetworkState decentralizedAuth = ClassTinkerers.getEnum(NetworkState.class, "DECENTRALIZEDAUTH");

      screen.connection.setPacketListener(new ClientDecentralizedAuthNetworkHandler(address, screen.connection, field_33738, screen.parent, screen::setStatus));
      screen.connection.send(new HandshakeC2SPacket(inetSocketAddress.getHostName(), inetSocketAddress.getPort(), decentralizedAuth));
      screen.connection.send(new DecentralizedAuthStartC2SPacket());
      info.cancel();
    }
  }
}
