package net.decentralizedauth.fabric.mixin;

import java.lang.reflect.Type;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;

import net.decentralizedauth.fabric.access.ServerMetadataAccess;
import net.minecraft.server.ServerMetadata;
import net.minecraft.util.JsonHelper;

@Mixin(ServerMetadata.Deserializer.class)
public abstract class ServerMetadataDeserializerMixin {
  @Inject(at = @At(value = "RETURN"), method = "deserialize", locals = LocalCapture.CAPTURE_FAILEXCEPTION)
  public void onDeserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext, CallbackInfoReturnable<ServerMetadata> info, JsonObject jsonObject, ServerMetadata serverMetadata) {
    if (jsonObject.has("decentralizedAuth")) {
      ((ServerMetadataAccess) serverMetadata).setDecentralizedAuthSupport(JsonHelper.getInt(jsonObject, "decentralizedAuth"));
    }
  }
  @Inject(at = @At(value = "RETURN"), method = "serialize", locals = LocalCapture.CAPTURE_FAILEXCEPTION)
  public void onSerialize(ServerMetadata serverMetadata, Type type, JsonSerializationContext jsonSerializationContext, CallbackInfoReturnable<JsonElement> info, JsonObject jsonObject) {
    if (((ServerMetadataAccess) serverMetadata).getDecentralizedAuthSupport() != null) {
      jsonObject.addProperty("decentralizedAuth", 1);
    }
  }
}
