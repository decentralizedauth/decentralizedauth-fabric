package net.decentralizedauth.fabric.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.decentralizedauth.fabric.server.ServerDecentralizedAuthNetworkHandler;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.listener.PacketListener;

@Mixin(ClientConnection.class)
public abstract class ClientConnectionMixin {
  @Shadow
  private PacketListener packetListener;

  @Inject(at = @At(value = "INVOKE", target = "net/minecraft/network/ClientConnection.sendQueuedPackets()V", shift = At.Shift.AFTER), method = "tick()V")
  public void onTick(CallbackInfo info) {
    if (this.packetListener instanceof ServerDecentralizedAuthNetworkHandler) {
      ((ServerDecentralizedAuthNetworkHandler)this.packetListener).tick();
    }
  }
}
