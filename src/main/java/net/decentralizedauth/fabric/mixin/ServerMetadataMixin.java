package net.decentralizedauth.fabric.mixin;

import org.checkerframework.common.aliasing.qual.Unique;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;

import net.decentralizedauth.fabric.access.ServerMetadataAccess;
import net.minecraft.server.ServerMetadata;

@Mixin(ServerMetadata.class)
public abstract class ServerMetadataMixin implements ServerMetadataAccess {
  @Nullable
  @Unique
  private Integer decentralizedAuthProtocol;

  @Override
  public void setDecentralizedAuthSupport(Integer protocol) {
    this.decentralizedAuthProtocol = protocol;
  }

  @Override
  @Nullable
  public Integer getDecentralizedAuthSupport() {
    return this.decentralizedAuthProtocol;
  }
}
