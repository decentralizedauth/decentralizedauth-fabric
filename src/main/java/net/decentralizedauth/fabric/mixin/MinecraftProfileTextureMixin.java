package net.decentralizedauth.fabric.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;

@Mixin(MinecraftProfileTexture.class)
public abstract class MinecraftProfileTextureMixin {
  @Shadow(remap = false)
  public abstract String getMetadata(final String key);

  @Inject(at = @At("HEAD"), method = "getHash", cancellable = true, remap = false)
  public void onGetHash(CallbackInfoReturnable<String> info) {
    String textureSha256 = this.getMetadata("textureSha256");
    if (textureSha256 != null) {
      info.setReturnValue(textureSha256);
      info.cancel();
    }
  }
}
