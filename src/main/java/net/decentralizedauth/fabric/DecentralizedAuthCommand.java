package net.decentralizedauth.fabric;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;
import java.util.UUID;

import com.mojang.authlib.GameProfile;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import com.mojang.brigadier.exceptions.SimpleCommandExceptionType;

import net.minecraft.command.CommandSource;
import net.minecraft.command.argument.GameProfileArgumentType;
import net.minecraft.server.PlayerManager;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.Text;

public class DecentralizedAuthCommand {
  private static final SimpleCommandExceptionType NO_PLAYERS_EXCEPTION = new SimpleCommandExceptionType(Text.of("No players found"));

  public static void register(CommandDispatcher<ServerCommandSource> dispatcher, boolean isDedicated) {
    dispatcher.register(
      CommandManager.literal("decentralizedauth").requires(source -> source.hasPermissionLevel(3))
        .then(CommandManager.literal("player")
          .executes(context -> DecentralizedAuthCommand.executePlayer(context.getSource(), Collections.singleton(context.getSource().getPlayerOrThrow().getGameProfile()))))
        .then(CommandManager.literal("player")
          .then(CommandManager.argument("targets", GameProfileArgumentType.gameProfile()).suggests((context, builder) -> {
            if (isDedicated) {
              PlayerManager playerManager = (context.getSource()).getServer().getPlayerManager();
              String[] onlinePlayerNames = playerManager.getPlayerNames();
              return CommandSource.suggestMatching(onlinePlayerNames, builder);
            } else {
              return CommandSource.suggestMatching(Collections.singleton(context.getSource().getPlayerOrThrow().getGameProfile().getName()), builder);
            }
          }).executes(context -> DecentralizedAuthCommand.executePlayer(context.getSource(), GameProfileArgumentType.getProfileArgument(context, "targets"))))
        )
    );
  }

  private static int executePlayer(ServerCommandSource source, Collection<GameProfile> targets) throws CommandSyntaxException {
    int i = 0;
    for (GameProfile gameProfile : targets) {
      UUID uuid = gameProfile.getId();
      String playerLoginType;
      switch (uuid.version()) {
        case 3:
          playerLoginType = "offline mode";
          break;
        case 4:
          playerLoginType = "Mojang";
          break;
        case 5:
          playerLoginType = "Decentralized Auth";
          break;
        default:
          playerLoginType = "unknown auth type";
      }
      source.sendFeedback(Text.of("UUID: " + uuid.toString()), false);
      source.sendFeedback(Text.of("Login type: " + playerLoginType), false);
      Optional<GameProfile> cached = source.getServer().getUserCache().getByUuid(gameProfile.getId());
      if (cached.isPresent()) {
        source.sendFeedback(Text.of("Last known name: " + cached.get().getName()), false);
      } else {
        source.sendFeedback(Text.of("No cached name data available"), false);
      }
      i += 1;
    }
    if (i == 0) {
      throw NO_PLAYERS_EXCEPTION.create();
    }
    return i;
  }
}
