package net.decentralizedauth.fabric.client;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.Validate;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.MinecraftClientAccess;
import net.decentralizedauth.fabric.util.DecentralizedAuthNetworkEncryptionUtils;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ServerAddress;
import net.minecraft.util.Util;
import net.minecraft.util.thread.TaskExecutor;

/**
 * Maps addresses of previously joined Minecraft servers to their Decentralized
 * Auth public identity keys.
 */
@Environment(value=EnvType.CLIENT)
public class DecentralizedAuthKnownServers {
  private static final Logger LOGGER = DecentralizedAuth.LOGGER;
  private static final TaskExecutor<Runnable> IO_EXECUTOR = TaskExecutor.create(Util.getMainWorkerExecutor(), "decentralizedauth-known_servers-io");
  private final MinecraftClient client;
  private final HashMap<String, Ed25519PublicKeyParameters> knownServers = new HashMap<String, Ed25519PublicKeyParameters>();

  public DecentralizedAuthKnownServers(MinecraftClient client) {
    this.client = client;
    this.loadFile();
  }

  public void loadFile() {
    try {
      this.knownServers.clear();
      File cacheFile = new File(((MinecraftClientAccess) this.client).getDecentralizedAuthClientDirectory(), "known_servers");
      if (!cacheFile.isFile()) {
        return;
      }
      List<String> lines = Files.readAllLines(cacheFile.toPath());
      for (String line : lines) {
        String[] segments = line.split(" ");
        Validate.validState(segments.length == 2, "known_servers file is formatted incorrectly", new Object[0]);
        String address = segments[0];
        Ed25519PublicKeyParameters identity = new Ed25519PublicKeyParameters(Base64.getDecoder().decode(segments[1].getBytes()));
        this.knownServers.put(address, identity);
      }
    }
    catch (Exception exception) {
      LOGGER.error("Couldn't load server list", exception);
    }
  }

  public void saveFile() {
    File clientDirectory = ((MinecraftClientAccess) this.client).getDecentralizedAuthClientDirectory();
    try {
      File tempFile = File.createTempFile("known_servers", "", clientDirectory);
      FileOutputStream output = new FileOutputStream(tempFile);
      for (Map.Entry<String, Ed25519PublicKeyParameters> entry : this.knownServers.entrySet()) {
        String line = entry.getKey() + " " + Base64.getEncoder().encodeToString(entry.getValue().getEncoded()) + "\n";
        output.write(line.getBytes());
      }
      output.close();
      File file2 = new File(clientDirectory, "known_servers_old");
      File file3 = new File(clientDirectory, "known_servers");
      Util.backupAndReplace(file3, tempFile, file2);
    }
    catch (Exception exception) {
      LOGGER.error("Couldn't save server list", exception);
    }
  }

  /**
   * {@return the identity public key for {@code address}, or {@code null} if there is no such one}
   */
  @Nullable
  public Ed25519PublicKeyParameters get(String address) {
    Validate.validState(!address.contains(" "), "Cannot get known_servers entry for address containing a space", new Object[0]);
    if (this.knownServers.containsKey(address)) {
      return this.knownServers.get(address);
    }
    return null;
  }

  /**
   * Removes the entry associated with the given server and saves it to disk.
   */
  public void remove(String address) {
    this.knownServers.remove(address);
    this.saveFile();
  }

  /**
   * Adds a new server identity to this list and saves it to disk.
   */
  public void add(String address, Ed25519PublicKeyParameters identity) {
    Validate.validState(!address.contains(" "), "Cannot add known_servers entry for address containing a space", new Object[0]);
    Validate.validState(!this.knownServers.containsKey(address), "Cannot add duplicate known_servers entry", new Object[0]);
    this.knownServers.put(address, identity);
    this.saveFile();
  }

  /**
   * Returns whether or not the client can connect to the given server.
   */
  public boolean verifyIdentity(ServerAddress address, Ed25519PublicKeyParameters claimedIdentity) {
    String addressString = address.getAddress() + ":" + String.valueOf(address.getPort());
    Ed25519PublicKeyParameters cachedIdentity = this.get(addressString);
    if (cachedIdentity == null) {
      IO_EXECUTOR.send(() -> {
        this.add(addressString, claimedIdentity);
      });
      return true;
    } else if (DecentralizedAuthNetworkEncryptionUtils.sameKey(cachedIdentity, claimedIdentity)) {
      return true;
    } else {
      return false;
    }
  }
}
