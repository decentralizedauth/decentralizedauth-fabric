package net.decentralizedauth.fabric.client;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.PlayerSkinProviderAccess;
import net.decentralizedauth.fabric.packet.PlayTextureResponseS2CPacket;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;

public class DecentralizedAuthClient implements ClientModInitializer {
  public void onInitializeClient() {
    ClientPlayNetworking.registerGlobalReceiver(DecentralizedAuth.TEXTURES_CHANNEL, (client, handler, buf, responseSender) -> {
      PlayTextureResponseS2CPacket packet = new PlayTextureResponseS2CPacket(buf);
      ((PlayerSkinProviderAccess) client.getSkinProvider()).onReceiveTextureFromServer(packet.getTextureData());
    });
  }
}
