package net.decentralizedauth.fabric.client;

import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthChallengeS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthCompressionS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthDisconnectS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthSuccessS2CPacket;
import net.minecraft.network.listener.PacketListener;

public interface ClientDecentralizedAuthPacketListener
extends PacketListener {
  public void onEncryptionRequest(DecentralizedAuthEncryptionRequestS2CPacket packet);

  public void onAuthChallenge(DecentralizedAuthAuthChallengeS2CPacket packet);

  public void onProfileRequest(DecentralizedAuthProfileRequestS2CPacket packet);

  public void onCompression(DecentralizedAuthCompressionS2CPacket packet);

  public void onSuccess(DecentralizedAuthSuccessS2CPacket packet);

  public void onDisconnect(DecentralizedAuthDisconnectS2CPacket packet);
}


