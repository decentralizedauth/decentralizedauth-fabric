package net.decentralizedauth.fabric.server;

import java.io.File;
import java.io.IOException;

import com.google.common.io.Files;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.MinecraftServerAccess;
import net.decentralizedauth.fabric.packet.PlayTextureRequestC2SPacket;
import net.decentralizedauth.fabric.packet.PlayTextureResponseS2CPacket;
import net.fabricmc.api.DedicatedServerModInitializer;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.network.PacketByteBuf;

public class DecentralizedAuthServer implements DedicatedServerModInitializer {
  public void onInitializeServer() {
    ServerPlayNetworking.registerGlobalReceiver(DecentralizedAuth.TEXTURES_CHANNEL, (server, player, handler, buf, responseSender) -> {
      PlayTextureRequestC2SPacket packet = new PlayTextureRequestC2SPacket(buf);
      String textureSha256 = packet.getTextureSha256();

      for (MinecraftProfileTexture.Type type : MinecraftProfileTexture.Type.values()) {
        if (type == MinecraftProfileTexture.Type.ELYTRA) {
          continue;
        }

        File cacheFile = ((MinecraftServerAccess) server).getServerTextureCachePath(type, textureSha256);

        if (cacheFile.isFile()) {
          byte[] textureData;
          try {
            textureData = Files.toByteArray(cacheFile);
          } catch (IOException e) {
            DecentralizedAuth.LOGGER.warn("Failed to read " + cacheFile.getPath(), e);
            continue;
          }

          PacketByteBuf responsePacket = PacketByteBufs.create();
          new PlayTextureResponseS2CPacket(textureData).write(responsePacket);
          ServerPlayNetworking.send(player, DecentralizedAuth.TEXTURES_CHANNEL, responsePacket);
          break;
        }
      }
    });
  }
}
