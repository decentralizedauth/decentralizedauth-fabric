package net.decentralizedauth.fabric.server;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.security.KeyPair;
import java.util.HashMap;
import java.util.HexFormat;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.crypto.SecretKey;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.Validate;
import org.bouncycastle.crypto.params.Ed25519PrivateKeyParameters;
import org.bouncycastle.crypto.params.Ed25519PublicKeyParameters;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.minecraft.MinecraftProfileTexture;

import net.decentralizedauth.fabric.DecentralizedAuth;
import net.decentralizedauth.fabric.access.MinecraftServerAccess;
import net.decentralizedauth.fabric.access.UserCacheAccess;
import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthChallengeS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthAuthProofC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthCompressionS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthDisconnectS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthEncryptionResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileRequestS2CPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthProfileResponseC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthStartC2SPacket;
import net.decentralizedauth.fabric.packet.DecentralizedAuthSuccessS2CPacket;
import net.decentralizedauth.fabric.util.DecentralizedAuthNetworkEncryptionUtils;
import net.decentralizedauth.fabric.util.DecentralizedAuthSkinUtils;
import net.decentralizedauth.fabric.util.UUIDv5;
import net.decentralizedauth.lib.server.DenyLoginException;
import net.decentralizedauth.lib.server.LoginHook;
import net.decentralizedauth.lib.server.ProfileLookup;
import net.minecraft.network.ClientConnection;
import net.minecraft.network.encryption.NetworkEncryptionException;
import net.minecraft.network.packet.s2c.play.DisconnectS2CPacket;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.MutableText;
import net.minecraft.text.Text;
import net.minecraft.util.TextifiedException;
import net.minecraft.util.UserCache;

public class ServerDecentralizedAuthNetworkHandler
implements ServerDecentralizedAuthPacketListener {
  static final Logger LOGGER = DecentralizedAuth.LOGGER;
  private static final int TIMEOUT_TICKS = 600;
  final MinecraftServer server;
  public final ClientConnection connection;
  State state = State.START;
  private int loginTicks;
  @Nullable
  GameProfile profile;
  @Nullable
  private ServerPlayerEntity delayedPlayer;
  private SecretKey sharedSecret;
  private final KeyPair ephemeralKeypair;

  public ServerDecentralizedAuthNetworkHandler(MinecraftServer server, ClientConnection connection) {
    this.server = server;
    this.connection = connection;
    this.ephemeralKeypair = DecentralizedAuthNetworkEncryptionUtils.generateX25519EphemeralKeypair();
  }

  public void acceptPlayer() {
    this.state = State.ACCEPTED;
    if (this.server.getNetworkCompressionThreshold() >= 0 && !this.connection.isLocal()) {
      this.connection.send(new DecentralizedAuthCompressionS2CPacket(this.server.getNetworkCompressionThreshold()), channelFuture -> this.connection.setCompressionThreshold(this.server.getNetworkCompressionThreshold(), true));
    }
    this.connection.send(new DecentralizedAuthSuccessS2CPacket(this.profile));
    ServerPlayerEntity serverPlayerEntity = this.server.getPlayerManager().getPlayer(this.profile.getId());
    try {
      ServerPlayerEntity serverPlayerEntity2 = this.server.getPlayerManager().createPlayer(this.profile, null);
      if (serverPlayerEntity != null) {
        this.state = State.DELAY_ACCEPT;
        this.delayedPlayer = serverPlayerEntity2;
      } else {
        this.addToServer(serverPlayerEntity2);
      }
    }
    catch (Exception exception) {
      LOGGER.error("Couldn't place player in world", exception);
      MutableText text2 = Text.translatable("multiplayer.disconnect.invalid_player_data");
      this.connection.send(new DisconnectS2CPacket(text2));
      this.connection.disconnect(text2);
    }
  }

  private void addToServer(ServerPlayerEntity player) {
      this.server.getPlayerManager().onPlayerConnect(this.connection, player);
  }

  @Override
  public void onDisconnected(Text reason) {
    LOGGER.info("{} lost connection: {}", (Object)this.getConnectionInfo(), (Object)reason.getString());
  }

  public void tick() {
    ServerPlayerEntity serverPlayerEntity;
    if (this.state == State.READY_TO_ACCEPT) {
      this.acceptPlayer();
    } else if (this.state == State.DELAY_ACCEPT && (serverPlayerEntity = this.server.getPlayerManager().getPlayer(this.profile.getId())) == null) {
      this.state = State.READY_TO_ACCEPT;
      this.addToServer(this.delayedPlayer);
      this.delayedPlayer = null;
    }
    if (this.loginTicks++ == TIMEOUT_TICKS) {
      this.disconnect(Text.translatable("multiplayer.disconnect.slow_login"));
    }
  }

  public String getConnectionInfo() {
    if (this.profile != null) {
      return this.profile + " (" + this.connection.getAddress() + ")";
    }
    return String.valueOf(this.connection.getAddress());
  }

  @Override
  public ClientConnection getConnection() {
    return this.connection;
  }

  public void disconnect(Text reason) {
    try {
      LOGGER.info("Disconnecting {}: {}", (Object)this.getConnectionInfo(), (Object)reason.getString());
      this.connection.send(new DecentralizedAuthDisconnectS2CPacket(reason));
      this.connection.disconnect(reason);
    }
    catch (Exception exception) {
      LOGGER.error("Error whilst disconnecting player", exception);
    }
  }

  @Override
  public void onStart(DecentralizedAuthStartC2SPacket packet) {
    Validate.validState(this.state == State.START, "Unexpected start packet", new Object[0]);
    if (this.server.isDedicated() && ((MinecraftServerAccess) this.server).getDecentralizedAuthIdentity() == null) {
      this.disconnect(Text.of("Server is not yet ready to accept Decentralized Auth connections - try again shortly!"));
      return;
    }
    GameProfile gameProfile = this.server.getHostProfile();
    if (gameProfile != null && connection.isLocal()) {
      this.profile = gameProfile;
      this.state = State.READY_TO_ACCEPT;
      return;
    }
    if (this.server.isOnlineMode() && !this.connection.isLocal()) {
      this.state = State.KEY;
      this.connection.send(new DecentralizedAuthEncryptionRequestS2CPacket(this.ephemeralKeypair.getPublic()));
    } else {
      this.state = State.READY_TO_ACCEPT;
    }
  }

  @Override
  public void onEncryptionResponse(DecentralizedAuthEncryptionResponseC2SPacket packet) {
    Validate.validState(this.state == State.KEY, "Unexpected key packet", new Object[0]);
    try {
      this.sharedSecret = DecentralizedAuthNetworkEncryptionUtils.finishX25519(this.ephemeralKeypair.getPrivate(), packet.getPublicKey());

      DecentralizedAuthNetworkEncryptionUtils.AESCiphers aesCiphers = DecentralizedAuthNetworkEncryptionUtils.deriveAESCiphers(this.sharedSecret);

      this.connection.setupEncryption(aesCiphers.decryptCipher(), aesCiphers.encryptCipher());
    }
    catch (NetworkEncryptionException e) {
      throw new IllegalStateException("Protocol error", e);
    }

    this.state = State.AUTHENTICATING;

    Ed25519PrivateKeyParameters serverIdentityPrivateKey = ((MinecraftServerAccess) this.server).getDecentralizedAuthIdentity();
    Ed25519PublicKeyParameters serverIdentityPublicKey = serverIdentityPrivateKey.generatePublicKey();

    byte[] signedSharedSecret = DecentralizedAuthNetworkEncryptionUtils.generateIdentityProof(serverIdentityPrivateKey, this.sharedSecret);

    this.connection.send(new DecentralizedAuthAuthChallengeS2CPacket(serverIdentityPublicKey, signedSharedSecret));
  }

  @Override
  public void onAuthProof(DecentralizedAuthAuthProofC2SPacket packet) {
    Validate.validState(this.state == State.AUTHENTICATING, "Unexpected auth proof packet", new Object[0]);

    Ed25519PublicKeyParameters clientIdentityPublicKey = packet.getClientIdentityPublicKey();
    if (!packet.verifySignedSharedSecret(this.sharedSecret)) {
      this.disconnect(Text.of("Failed to validate client identity!"));
      return;
    }

    final UUID playerUUID = UUIDv5.playerUUIDFromClientPubkey(clientIdentityPublicKey);

    Optional<GameProfile> previousLogin = this.server.getUserCache().getByUuid(playerUUID);

    this.profile = previousLogin.orElse(new GameProfile(playerUUID, null));

    // Enforce bans by UUID or IP address before requesting profile information
    Text text;
    if ((text = this.server.getPlayerManager().checkCanJoin(this.connection.getAddress(), this.profile)) != null) {
      this.disconnect(text);
    }

    this.state = State.PROFILE;
    this.connection.send(new DecentralizedAuthProfileRequestS2CPacket(previousLogin));
  }

  enum OfficialAccountPrecedence {
    /**
     * Never allow a Decentralized Auth player to join the server if any Mojang
     * account currently has the same username
     */
    STRICT,
    /**
     * Allow a Decentralized Auth player to join with any username as long as
     * no Mojang account with that name has previously joined
     */
    PREFER,
    /**
     * Don't give Mojang accounts any special treatment, at the risk of locking
     * out accounts
     */
    NONE;
  }

  private class DefaultLoginHookImpl implements LoginHook {
    public DefaultLoginHookImpl() {}

    @Override
    public GameProfile setupPlayer(GameProfile requestedProfile, ProfileLookup profileLookup) throws DenyLoginException {
      // TODO add config options
      boolean allowRename = true;
      OfficialAccountPrecedence precedence = OfficialAccountPrecedence.PREFER;

      if (!LoginHook.validVanillaUsername(requestedProfile.getName())) {
        throw new DenyLoginException("Invalid username requested");
      }

      Optional<GameProfile> previousLogin = profileLookup.cachedLoginForUUID(requestedProfile.getId());
      Optional<GameProfile> sameNameLogin = profileLookup.cachedLoginWithName(requestedProfile.getName());

      if (precedence == OfficialAccountPrecedence.STRICT) {
        Optional<GameProfile> sameNameMojangAccount = profileLookup.mojangProfileWithName(requestedProfile.getName());

        // Check that an official account hasn't renamed to the cached name,
        // and if so, remove the cached entry from the `UserCache`
        if (previousLogin.isPresent() && sameNameMojangAccount.isPresent()) {
          profileLookup.evictCacheEntry(requestedProfile.getId());
          previousLogin = Optional.empty();
        }

        if (sameNameLogin.isEmpty()) {
          sameNameLogin = sameNameMojangAccount;
        }
      }

      if (!allowRename && previousLogin.isPresent() && previousLogin.get().getName() != null && !previousLogin.get().getName().equals(requestedProfile.getName())) {
        throw new DenyLoginException("Server does not allow renames, please login using your previous username '" + previousLogin.get().getName() + "'");
      }

      // TODO evict cache entries on official account logins in PREFER mode

      if (sameNameLogin.isPresent() && !sameNameLogin.get().getId().equals(requestedProfile.getId())) {
        throw new DenyLoginException("Your requested username is already in use by another account. Please choose a different username.");
      }

      return requestedProfile;
    }
  }

  private class ProfileLookupImpl implements ProfileLookup {
    private final UserCache userCache;

    public ProfileLookupImpl(UserCache userCache) {
      this.userCache = userCache;
    }

    @Override
    public Optional<GameProfile> cachedLoginForUUID(UUID id) {
      return this.userCache.getByUuid(id);
    }

    @Override
    public Optional<GameProfile> cachedLoginWithName(String name) {
      return ((UserCacheAccess) this.userCache).findCachedByName(name);
    }

    @Override
    public Optional<GameProfile> mojangProfileWithName(String name) {
      return this.userCache.findByName(name);
    }

    @Override
    public void evictCacheEntry(UUID id) {
      ((UserCacheAccess) this.userCache).removeByUUID(id);
    }
  }

  @Override
  public void onProfileResponse(DecentralizedAuthProfileResponseC2SPacket packet) {
    Validate.validState(this.state == State.PROFILE, "Unexpected profile packet", new Object[0]);

    GameProfile requestedProfile = packet.getProfile();
    if (!this.profile.getId().equals(requestedProfile.getId())) {
      this.disconnect(Text.of("Unauthorized UUID requested"));
      return;
    }

    ProfileLookup profileLookup = new ProfileLookupImpl(this.server.getUserCache());

    GameProfile preparedProfile;
    try {
      preparedProfile = new DefaultLoginHookImpl().setupPlayer(requestedProfile, profileLookup);
    } catch (DenyLoginException e) {
      this.disconnect(Text.of(e.getMessage()));
      return;
    }

    Map<MinecraftProfileTexture.Type, MinecraftProfileTexture> requestedTextures = this.server.getSessionService().getTextures(requestedProfile, false);
    Map<MinecraftProfileTexture.Type, byte[]> textureDataMap = packet.getTextureData();

    Map<MinecraftProfileTexture.Type, String> validTextureHashes = new HashMap<MinecraftProfileTexture.Type, String>();

    for (Map.Entry<MinecraftProfileTexture.Type, MinecraftProfileTexture> textureEntry : requestedTextures.entrySet()) {
      MinecraftProfileTexture.Type type = textureEntry.getKey();
      MinecraftProfileTexture texture = textureEntry.getValue();

      if (type == MinecraftProfileTexture.Type.ELYTRA) {
        this.disconnect(Text.of("Invalid texture type 'ELYTRA'"));
        return;
      }

      String requestedSha256String = texture.getMetadata("textureSha256");
      try {
        byte[] requestedSha256 = HexFormat.of().withLowerCase().parseHex(requestedSha256String);
        Validate.validState(requestedSha256.length == 32);
      } catch(Exception e) {
        this.disconnect(Text.of("Invalid SHA256 hash supplied: '" + requestedSha256String + "'"));
      }

      File cacheFile = ((MinecraftServerAccess) this.server).getServerTextureCachePath(type, requestedSha256String);

      boolean hasCachedTexture = false;

      if (cacheFile.isFile()) {
        hasCachedTexture = true;
      } else {
        // Enforce that the response packet contains the texture
        if (!textureDataMap.containsKey(type)) {
          this.disconnect(Text.of("Missing requested texture for " + type.name()));
          return;
        }
        byte[] textureData = textureDataMap.get(type);

        // Verify that the data matches to the requested hash
        final String dataSha256 = DecentralizedAuthSkinUtils.sha256(textureData);
        if (!dataSha256.equals(requestedSha256String)) {
          this.disconnect(Text.of("Requested " + type.name() + " texture does not match requested hash"));
          return;
        }

        // And that it's a well-formed image
        try {
          DecentralizedAuthSkinUtils.validateTexture(type, textureData);
        } catch (Exception e) {
          this.disconnect(Text.of("Invalid PNG supplied for " + type.name() + " texture"));
          return;
        }

        // Finally, cache the texture
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(textureData)) {
          FileUtils.copyInputStreamToFile(inputStream, cacheFile);
          hasCachedTexture = true;
        } catch (IOException e) {
          LOGGER.warn("Could not save requested data to disk");
          e.printStackTrace();
        }
      }

      if (hasCachedTexture) {
        validTextureHashes.put(type, requestedSha256String);
      }
    }

    this.profile = DecentralizedAuthSkinUtils.assembleProfile(preparedProfile.getId(), preparedProfile.getName(), validTextureHashes);

    this.state = State.READY_TO_ACCEPT;
  }

  static enum State {
    START,
    KEY,
    AUTHENTICATING,
    PROFILE,
    READY_TO_ACCEPT,
    DELAY_ACCEPT,
    ACCEPTED;
  }

  static class LoginException
  extends TextifiedException {
    public LoginException(Text text) {
      super(text);
    }

    public LoginException(Text text, Throwable throwable) {
      super(text, throwable);
    }
  }
}
