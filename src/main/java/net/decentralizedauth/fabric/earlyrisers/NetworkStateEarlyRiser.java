package net.decentralizedauth.fabric.earlyrisers;

import com.chocohead.mm.api.ClassTinkerers;

import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.MappingResolver;
import net.minecraft.network.NetworkState;

public class NetworkStateEarlyRiser implements Runnable {
  @Override
  public void run() {
    FabricLoader loader = FabricLoader.getInstance();
    MappingResolver remapper = loader.getMappingResolver();

    String networkStateEnum;
    if (loader.isDevelopmentEnvironment()) {
      networkStateEnum = remapper.mapClassName("intermediary", "net.minecraft.network.NetworkState");
    } else {
      networkStateEnum = remapper.mapClassName("intermediary", "net.minecraft.class_2539");
    }

    String networkStatePacketHandlerInitializerClass;
    if (loader.isDevelopmentEnvironment()) {
      networkStatePacketHandlerInitializerClass = remapper.mapClassName("intermediary", "net.minecraft.network.NetworkState$PacketHandlerInitializer");
    } else {
      networkStatePacketHandlerInitializerClass = remapper.mapClassName("intermediary", "net.minecraft.class_2539$class_4533");
    }
    ClassTinkerers.enumBuilder(networkStateEnum, int.class, "L" + networkStatePacketHandlerInitializerClass + ";")
      .addEnum("DECENTRALIZEDAUTH", this::createDecentralizedAuthEnumVariant)
      .build();
  }

  Object[] createDecentralizedAuthEnumVariant() {
    return new Object[] {
      69,
      // We provide an empty PacketHandlerInitializer here and call `setup` in
      // the `NetworkState` constructor to avoid re-entrance issues.
      new NetworkState.PacketHandlerInitializer()
    };
  }
}
